#!/bin/sh

sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y ;
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo ;

sudo dnf update -y && flatpak update ;

flatpak install com.vscodium.codium -y
flatpak install com.github.tchx84.Flatseal -y
flatpak install flathub fr.romainvigier.MetadataCleaner -y
flatpak install flathub com.github.micahflee.torbrowser-launcher -y
flatpak install flathub com.mattjakeman.ExtensionManager -y
flatpak install flathub org.telegram.desktop -y

sudo dnf install libvirt restic cargo clang chromium opencl-headers sshuttle pdfgrep thunderbird borgbackup gstreamer1-vaapi gcolor3 python3-pyperclip google-roboto-fonts ShellCheck pipenv python3-nose-testconfig python3-isort dnscrypt-proxy tldr docker-compose sl croc wget2 yt-dlp torsocks xclip qrencode g++ evolution minder cmake lm_sensors-libs zstd zulucrypt qbittorrent flatpak-selinux distrobox rclone python3-devel python3-virtualenv gnome-extensions-app srm pixiewps megatools fontawesome-fonts yarnpkg nodejs testdisk cheat emacs ripgrep file-roller-nautilus ngrep httpie openssl lsd sway rescene age chkrootkit drawing rkhunter fzf git fd-find ncdu android-tools gnome-feeds gtkhash java-11-openjdk-devel docker yaru-gtk4-theme yaru-icon-theme celluloid mpv aria2 foliate lynx speedtest-cli vim gimp zsh dino bat neofetch gnome-tweaks neovim @virtualization okular toolbox podman feh htop ruby go tor nmap gparted keepassxc kleopatra pwgen wireshark bleachbit epiphany telnet gparted ftp dash weechat aircrack-ng

# Install codecs after rpm-fusion
sudo dnf group upgrade --with-optional --allowerasing Multimedia

podman pull docker.io/library/ubuntu ;
podman pull docker.io/library/fedora ;
podman pull docker.io/library/alpine ;

# WezTerm
mkdir ~/.config/wezterm ;
cp ../.config/wezterm/wezterm.lua ~/.config/wezterm ;

# NeoVim
mkdir ~/.config/nvim ;
cp ../.config/init.vim ~/.config/nvim/ ;

# MPV
cp ../.config/mpv.conf ~/.config/mpv/ ;

# htop
mkdir ~/.config/htop/
cp ../.config/htoprc ~/.config/htop/ ;

sudo dnf update -y && flatpak update ;

# OhMyZsh
echo "zsh" >> ~/.bashrc ;

# Install vim-plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim' ;

# OhMyZsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)";
